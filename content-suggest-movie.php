<?php
if( isset($_POST) ){
    $formok = true;
    $errors = array();

    $ipaddress = $_SERVER['REMOTE_ADDR'];
    $date = date('d/m/Y');
    $time = date('H:i:s');

    $imdbid = $_POST['imdbid'];
    $reason = $_POST['reason'];

    if(empty($imdbid)){
        $formok = false;
        $errors[] = "You have not entered an IMDB ID";
    }
    elseif(preg_match("/tt\\d{7}/", $imdbid) == 0){
        $formok = false;
        $errors[] = "Please provide a correct IMDB ID format";
    }

    if(empty($reason)){
        $formok = false;
        $errors[] = "You have not entered a reason";
    }
    elseif(strlen($reason) < 100){
        $formok = false;
        $errors[] = "Sorry but we don't feel that's a LONG enough reason";
    }

    if($formok){

        global $user_ID;
        $new_post = array(
            'post_title' => 'suggestion:' . $imdbid . ':' . $date . ':' . $time,
            'post_content' => $reason,
            'post_status' => 'pending',
            'post_date' => date('Y-m-d H:i:s'),
            'post_type' => 'events'
        );
        $post_id = wp_insert_post($new_post);
        add_post_meta( $post_id, '_imdb', $imdbid) || update_post_meta( $post_id, '_imdb', $imdbid );
    }
    $returndata = array(
        'posted_form_data' => array(
            'imdbid' => $imdbid,
            'reason' => $reason
        ),
        'form_ok' => $formok,
        'errors' => $errors
    );


    //if this is not an ajax request
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest'){
        //set session variables
        session_start();
        $_SESSION['cf_returndata'] = $returndata;

        //redirect back to form
//        header('location: ' . $_SERVER['HTTP_REFERER']);
    }
}
?>
<?php if ( is_user_logged_in() ):?>

<div id="contact-form" class="clearfix">
    <h1>Suggest a movie!</h1>
    <h2>Fill out this form to suggest a movie to be considered by the staff! Please provide a detailed reason for this movie to be included</h2>
    <?php
    //init variables
    $cf = array();
    $sr = false;

    if(isset($_SESSION['cf_returndata'])){
        $cf = $_SESSION['cf_returndata'];

        if($cf['posted_form_data']['reason'] && $cf['posted_form_data']['imdbid'])
            $sr = true;
    }
    ?>
    <ul id="errors" class="<?php echo ($sr && !$cf['form_ok']) ? 'visible' : ''; ?>">
        <li id="info">There were some problems with your form submission:</li>
        <?php
        if(isset($cf['errors']) && count($cf['errors']) > 0) :
            foreach($cf['errors'] as $error) :
                ?>
                <li><?php echo $error ?></li>
            <?php
            endforeach;
        endif;
        ?>
    </ul>
    <p id="success" class="<?php echo ($sr && $cf['form_ok']) ? 'visible' : ''; ?>">Thanks for your message! We will get back to you ASAP!</p>

    <form method="post">
        <label for="imdbid">IMDB ID <span class="required">*</span></label>
        <input type="text" id="imdbid" name="imdbid"
               value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['imdbid'] : '' ?>"
               placeholder="tt2561572" required="required" autofocus="autofocus" />

        <label for="reason">Why? <span class="required">*</span></label>
        <textarea id="reason" name="reason" placeholder="Tell us why this movie is special..."
                  required="required" data-minlength="100"><?php echo ($sr && !$cf['form_ok'])
                                                                    ? $cf['posted_form_data']['reason']
                                                                    : '' ?></textarea>

        <span id="loading"></span>
        <input type="submit" value="Suggest!" id="submit-button" />
        <p id="req-field-desc"><span class="required">*</span> indicates a required field</p>
    </form>
</div>

<?php elseif( !is_user_logged_in() ): ?>
        <div>Please log in to use this feature...</div>
<?php endif; ?>