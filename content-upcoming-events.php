
<?php
$today = date('Y/m/d');
$args = array(
    'post_type' => 'events',
    'meta_query' => array('relation' => 'AND',
        array('relation' => 'OR',
            'key' => '_date',
            'compare' => '>=',
            'value' => $today
        ),
        array(
            'key' => '_is_candidate',
            'compare' => 'NOT EXISTS'
        )
    ),
    'orderby' => '_date',
    'order' => 'ASC'
);

$upcoming = new WP_Query( $args );
?>

<div class="content-column-1 float-container leading trailing">
    <h1>Planned Upcoming Events</h1>
    <hr/>


    <div class="planned-events content-column-1 float-container">
        <?php
        while ( $upcoming->have_posts() ) : $upcoming->the_post();
            get_template_part( 'content', 'planned' );
        endwhile;
        ?>
    </div>
</div>

<?php
$args2 = array(
    'post_type' => 'events',
    'meta_query' => array(
        array(
            'key' => '_is_candidate',
            'value' => 'on'
        )
    )
);

$candidates = new WP_Query( $args2 );
?>
<div class="content-column-1 float-container leading trailing">
    <h1>Event Candidates</h1>

    <hr/>

    <div class="event-candidates content-column-1 float-container">
        <?php
        while ( $candidates->have_posts() ) : $candidates->the_post();
            get_template_part( 'content', 'candidate' );
        endwhile;
        ?>
    </div>
</div>