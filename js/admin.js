/**
 * Created by RasmusZimmer on 21-03-2015.
 */
jQuery(document).ready(function($){

    // Events form handling
    var $isCandidateChkBox = $("#ituf_events_is_candidate").find("input[type=checkbox]"),
        $dateBox = $("#ituf_events_date").find("input[type=text]"),
        $venueBox = $("#ituf_events_venue").find("input[type=text]"),
        $descBox = $("#ituf_events_desc").find("textarea");

    if($isCandidateChkBox.is(":checked")){
        $dateBox.prop( "disabled", true);
        $venueBox.prop( "disabled", true);
        $descBox.prop( "disabled", true);
    }

    $isCandidateChkBox.change(function() {
        $dateBox.prop( "disabled", this.checked );
        $venueBox.prop( "disabled", this.checked );
        $descBox.prop( "disabled", this.checked );
    });

    $("#event-date").datepicker({
        dateFormat: "yy/mm/dd"
    });

    // Reset votes handling
    $('#admin-reset-votes').click(function() {
        $.post(
            localizedVars.ajaxurl,
            {
                // wp ajax action
                action : 'clear-votes',

                // send the nonce along with the request
                nonce : localizedVars.nonce
            },
            function( response ) {
                if(response.votes) {
                    $("table.admin-votes .num-votes").each(function(i) {
                        $(this).text(0);
                    });
                }
            }
        );
    })
});