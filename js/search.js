/**
 * Created by RasmusZimmer on 01-04-2015.
 */
require(['jquery', 'knockout'], function($, ko){

    _latestResults = null;
    _searchReq = null;

    function SearchVM () {
        var self = this;

        this.searchString = ko.observable();
        this.prevSearchString = "";
    }

    function ResultsVM (){
        var self = this;

        this.results = ko.observableArray();
        this.hasResults = ko.computed(function() {
            return  self.results() && self.results().length > 0;
        });
        this.isCandidate = function(data) {
            return data.isCandidate === 'on';
        };
        this.animationDone = function(elem) {
            $(elem).find("li.search-result a").first().focus();
        }
    }

    function queryServer(searchTerm, vm){
        _searchReq = $.get(localizedVars.ajaxurl, {
            action : 'search',
            searchTerm: searchTerm
        }).done(function(response) {
            vm.results(response.results);
        });
    }

    function adjustResultsBox(){
        var searchBoxOffset = $('#event-search input.search-box').offset(),
            $resultsWrap = $('.search-results-wrapper'),
            adminbarHeight = $('#wpadminbar').height();

        $resultsWrap.css({
            left: searchBoxOffset.left,
            top: 59+adminbarHeight+'px'
        });
    }

    $(function () {
        var searchVM = new SearchVM();
        var resultsVM = new ResultsVM();

        searchVM.searchString.subscribe(function(val) {
            if(val)
                searchVM.prevSearchString = val;
        }, null, "beforeChange");

        searchVM.searchString.subscribe(function(val) {
            if(val.length < 3) {
                resultsVM.results([]);
                return;
            }

            queryServer(val, resultsVM);
        });

        resultsVM.hasResults.subscribe(function(hasResults) {
            if(hasResults){
                adjustResultsBox();
            }
        });

        ko.applyBindings(searchVM, $('#event-search')[0]);
        ko.applyBindings(resultsVM, $('.search-results-wrapper')[0]);

        $('.search-results-wrapper').on('keydown', 'li', function(e) {
            $this = $(this);
            if (e.keyCode == 40) {
                $this.next().find('a').focus();
                return false;
            } else if (e.keyCode == 38) {
                var $prev = $this.prev().find('a');
                //debugger;
                if($prev.length !== 0){
                    $prev.focus();
                }else{
                    $('input.search-box').focus();
                }
                return false;
            }
        });

        $('.events-search-wrapper').on('keydown', function(e) {
            $this = $(this);
            if (e.keyCode == 40) {
                $('li.search-result a').first().focus();
                return false;
            }
        });

        $(window).resize(function() {
            adjustResultsBox();
        })
    });
});