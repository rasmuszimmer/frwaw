/**
 * Created by RasmusZimmer on 28-03-2015.
 */
require(['jquery'], function($){
    $(document).ready(function(){
        $('.js-voting').click(function(){
            var $self = $(this);

            if($self.hasClass("disabled")){
                return false;
            }

            var postID = $self.attr('id').split('-')[0];
            $.post( localizedVars.ajaxurl, {
                    action : 'movie-vote',
                    postID : postID,
                    nonce : localizedVars.nonce
                }, function( response ) {
                    if(response.change && postID === response.change.new.id){
                        $self.addClass("disabled");
                        $self.attr("title", 'You already voted for this title');
                        $('.js-voting').each(function(i) {
                            if(this !== $self[0]){
                                $(this).removeClass('disabled');
                                $(this).attr("title", 'Vote for this title');
                            }
                        })
                    }
                }
            );
            return false;
        });
    });
})

