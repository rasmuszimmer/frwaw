/**
 * Created by RasmusZimmer on 24-03-2015.
 */

define(['jquery', 'underscore', 'knockout'], function ($, _, ko) {
    this.DataModel = new (function() {

        var _bindingTasksCount = 0;
        var _vmsDone = 0;
        var _storage = [];

        // Setup API key and the config object needed from TMDB for composing asset paths
        var _tmdbApiKey = '0143aac658ba7c7bd8536dd58bd4c1d3',
            _tmdbConfigUrl = 'http://api.themoviedb.org/3/configuration?api_key=' + _tmdbApiKey,
            _getTmdbConfigData = $.get(_tmdbConfigUrl).promise();

        // Classes
        function SingleEventVM() {
            var self = this;

            this.dataTasksCount = 0;

            this.finishTask = function(){
                self.dataTasksDone(self.dataTasksDone()+1);
            };

            this.dataTasksDone = ko.observable(0);
            this.donePopulating = ko.computed(function(){
                var done = self.dataTasksDone() === self.dataTasksCount && self.dataTasksCount > 0;
                if(done){
                    _vmsDone++;
                    if(_vmsDone === _bindingTasksCount){
                        saveData();
                    }
                }
                return done;
            });

            // Normals
            this.actors = ko.observableArray();
            this.director = ko.observable();
            this.genre = ko.observable();
            this.language = ko.observable();
            this.metaScore = ko.observable();
            this.plot = ko.observable();
            this.rated = ko.observable();
            this.runtime = ko.observable();
            this.title = ko.observable();
            this.writers = ko.observableArray();
            this.year = ko.observable();
            this.imdbRating = ko.observable();
            this.tomatoImage = ko.observable();
            this.tomatoMeter = ko.observable();
            this.synopsis = ko.observable();
            this.posterUrl = ko.observable();
            this.backdropUrl = ko.observable();


            // Computeds
            this.getBackgroundImage = ko.pureComputed(function(){
                return self.backdropUrl()
                    ? 'url(' + self.backdropUrl() + ')'
                    : 'url(' +imagesPath + 'curtain.jpg)'
            });
            this.getTomatoMeter = ko.pureComputed(function(){
                return (self.tomatoMeter() === 'N/A' || !self.tomatoMeter())
                    ? 'N/A'
                    : self.tomatoMeter() + ' %'
            });
            this.getMetascore = ko.pureComputed(function(){
                return (self.metaScore() === 'N/A' || !self.metaScore())
                    ? 'N/A'
                    : self.metaScore() + ' / 100'
            });
            this.getRottenImage = ko.pureComputed(function () {
                var defaultImgPath = imagesPath + 'fresh.png';
                switch(self.tomatoImage()) {
                    case 'certified':
                        return imagesPath + 'CF_300x300.png';
                        break;
                    case 'fresh':
                        return defaultImgPath;
                        break;
                    case 'rotten':
                        return imagesPath + 'rotten.png';
                        break;
                    default:
                        return defaultImgPath;
                }
            })
        }

        // Private functions
        function tryParse(data){
            if(_.isString(data)){
                return JSON.parse(data);
            }
            return data;
        }

        function bindOmdbBaseData(vm, data){
            vm.actors(data.Actors.split(','));
            vm.director(data.Director);
            vm.genre(data.Genre);
            vm.language(data.Language);
            vm.metaScore(data.Metascore);
            vm.rated(data.Rated);
            vm.runtime(data.Runtime);
            vm.title(data.Title);
            vm.writers(data.Writer.split(','));
            vm.year(data.Year);
            vm.imdbRating(data.imdbRating);
            vm.tomatoMeter(data.tomatoMeter);
            vm.tomatoImage(data.tomatoImage);
            vm.finishTask();
        }

        function startMovieDataTask (imdbID, vm, apiOptions){
            if(!imdbID) return;

            var options = jQuery.extend({}, {
                // Defaults
                posterSize: 'w500',
                backdropSize: 'w1280',
                trailerCount: 3,
                ratings: true,
                shortPlot: true,
                fullPlot: true,
                poster: true,
                backdrop: true
            },apiOptions) ;

            var omdbUrl = 'http://www.omdbapi.com/?i=' + imdbID + '&tomatoes=true',
                omdbFullPlotUrl = 'http://www.omdbapi.com/?i='+ imdbID + '&tomatoes=true&plot=full',
                tmdbUrl = 'http://api.themoviedb.org/3/find/'+ imdbID +'?api_key='
                    + _tmdbApiKey + '&external_source=imdb_id';

            if(options.backdrop || options.poster){
                vm.dataTasksCount++;
                var getTmdbData = $.get(tmdbUrl).promise();
                $.when(_getTmdbConfigData, getTmdbData)
                    .done(function(tmdbConfigDataRes, tmdbDataRes) {
                        var tmdbConfig = _.first(tmdbConfigDataRes),
                            tmdbData = _.first(tmdbDataRes),
                            tmdbBaseUrl = tmdbConfig.images.base_url,
                            movieData = _.first(tmdbData.movie_results);

                        var title = movieData.title,
                            year = _.first(movieData.release_date.split('-'));

                        //add to storage obj
                        _storage.push({
                            imdbID: imdbID,
                            title: title,
                            year: year
                        });

                        vm.title(title);
                        vm.year(year);
                        vm.backdropUrl(tmdbBaseUrl + options.backdropSize + movieData.backdrop_path);
                        vm.posterUrl(tmdbBaseUrl + options.posterSize + movieData.poster_path);
                        vm.finishTask();
                    });
            }

            if(options.shortPlot){
                vm.dataTasksCount++;
                $.get(omdbUrl).done(function(omdbRes) {
                    var data = tryParse(omdbRes);

                    //add to storage
                    _storage.push({
                        imdbID: imdbID,
                        title: data.Title,
                        year: data.Year
                    });

                    bindOmdbBaseData(vm, data);
                    vm.plot(data.Plot);
                })
            }
            if(options.fullPlot) {
                vm.dataTasksCount++;
                $.get(omdbFullPlotUrl).done(function(omdbFullPlotRes) {
                    var data = tryParse(omdbFullPlotRes);

                    //add to storage obj
                    _storage.push({
                        imdbID: imdbID,
                        title: data.Title,
                        year: data.Year
                    });

                    bindOmdbBaseData(vm, data);
                    vm.synopsis(data.Plot);
                });
            }
        }

        function saveData(){
            $.post(
                localizedVars.ajaxurl,
                {
                    // wp ajax action
                    action : 'save_omdb_data',
                    data: _storage,
                    // send the nonce along with the request
                    nonce : localizedVars.nonce
                },
                function( response ) {
                    _bindingTasksCount = 0;
                    _vmsDone= 0;
                    _storage = [];
                }
            );
        }

        // Public functions
        this.AddBindingTask = function(imdbID, selectorID, apiOptions){
            _bindingTasksCount++;
            var vm = new SingleEventVM();
            startMovieDataTask(imdbID, vm, apiOptions);

            // Ensure DOM ready before binding
            $(function () {
                ko.applyBindings(vm, $('#'+selectorID)[0]);
            });

        }
    })();

    return DataModel;
});

