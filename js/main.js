/**
 * Created by RasmusZimmer on 26-03-2015.
 */
require.config({
    baseUrl: jsPath,
    paths: {
    underscore: 'underscore/underscore-min',
        knockout: 'knockout-3.3.0',
        jquery: 'jquery-2.1.3.min',
        jqueryUI: 'jquery_ui/jquery-ui.min',
        movieDataFetcher: 'movie-data-fetcher',
        blockUI: 'jquery-plugins/jquery.blockUI'
    }
});

require(["blockUI"]);
require(["jqueryUI"]);
require(["movieDataFetcher"]);

// Setup custom knockout binding handlers
require(["knockout"], function(ko) {

    // Additional knockout bindingHandlers
    ko.bindingHandlers.fadeVisible = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            $(element).toggle(ko.unwrap(value));
        },
        update: function(element, valueAccessor) {
            var value = valueAccessor();
            ko.unwrap(value) ? $(element).fadeIn(1000) : $(element).fadeOut(1000);
        }
    };

    ko.bindingHandlers.slideVisible = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            $(element).toggle(ko.unwrap(value));
        },
        update: function(element, valueAccessor, allBindings) {
            var value = valueAccessor(),
                valUnwrapped = ko.unwrap(value),
                duration = allBindings.get('duration') || 400,
                callback = allBindings.get('callback');

            valUnwrapped ? _.defer(function() {
                $(element).slideDown(duration, function() {
                    if(typeof  callback === 'function'){
                        callback(element);
                    }
                });
            }) : _.defer(function() {
                $(element).slideUp(duration)
            });
        }
    };

    ko.bindingHandlers.blockUI = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            $(element)[ko.unwrap(value) ? 'unblock' : 'block']({
                message: '<img src="http://razi.frwaw.itu.dk/wp-content/themes/ITUFilm/images/ajax-loader.gif" />'
            });
        },
        update: function(element, valueAccessor) {
            var value = valueAccessor();
            $(element)[ko.unwrap(value) ? 'unblock' : 'block']({
                message: '<img src="http://razi.frwaw.itu.dk/wp-content/themes/ITUFilm/images/ajax-loader.gif" />'
            });
        }
    };

    ko.bindingHandlers.stopBinding = {
        init: function() {
            return { controlsDescendantBindings: true };
        }
    };

    ko.virtualElements.allowedBindings.stopBinding = true;
});