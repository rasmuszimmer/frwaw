<?php

$date = get_post_meta($post->ID, "_date", true);
$time = get_post_meta($post->ID, "_time", true);
$venue = get_post_meta($post->ID, "_venue", true);
$imdb = get_post_meta($post->ID, "_imdb", true);
$desc = get_post_meta($post->ID, "_desc", true);
$formattedDate = date("F j", strtotime($date));
$formattedTime = date('g:i A', strtotime($time));

$args = array(
    'post_type' => 'review',
    'meta_key' => '_imdb',
    'meta_compare' => '=',
    'meta_value' => $imdb,
    'orderby' => 'date',
    'order' => 'ASC'
);

$rating_sum = 0;
$reviews = get_posts($args);

foreach ($reviews as $review) {
    $rating = get_post_meta($review->ID, '_rating', true);
    $rating_sum += $rating;
}

$avg_rating = $rating_sum / count($reviews);
$avg_rating = round_to_nearest_half($avg_rating);
$avg_rating *= 10;

?>
    <div class="content-column-1 float-container leading">

        <?php if(is_page('next-showing')) : ?>
            <div class="content-column-1 leading">
                <?php the_content() ?>
            </div>
        <?php endif; ?>
        <!-- Poster -->
        <div class="alignleft content-column-2 leading">
            <div class="movie-poster">
                <?php if(is_page('next-showing')) : ?>
                    <a href="<?php echo get_permalink()?>">
                        <img class="image lg" data-bind="attr: {src: posterUrl}"/>
                    </a>
                <?php else : ?>
                    <img data-bind="attr: {src: posterUrl}"/>
                <?php endif; ?>
            </div>
        </div> <!-- Poster -->

        <!-- Movie Info-->
        <div class="alignright content-column-2 content-collected float-container">
            <div class="content-column-1">
                <h2 data-bind="text: title">
            </div>
            <div class="movie-info">
                <div class="alignleft content-column-2">
                    <div class="info-row">
                        <span class="info-label">Year</span>
                        <span data-bind="text: year"></span>
                    </div>
                    <div class="info-row">
                        <span class="info-label">Runtime</span>
                        <span data-bind="text: runtime"></span>
                    </div>

                </div>
                <div class="alignright content-column-2">
                    <div class="info-row">
                        <span class="info-label">Language</span>
                        <span data-bind="text: language"></span>
                    </div>
                    <div class="info-row">
                        <span class="info-label">Rated</span>
                        <span data-bind="text: rated"></span>
                    </div>
                </div>

            </div>
            <div class="movie-people content-column-1 float-container">
                <div class="alignleft content-column-2 leading">
                    <div class="info-row">
                        <span class="info-label">Director</span>
                        <span data-bind="text: director"></span>
                    </div>
                    <div class="info-row">
                        <span class="info-label">Writers</span>
                        <!-- ko foreach: writers -->
                        <span style="display: block" data-bind="text: $data"></span>
                        <!-- /ko -->
                    </div>
                </div>
                <div class="alignright content-column-2">
                    <div class="info-row">
                        <span class="info-label">Actors</span>
                        <!-- ko foreach: actors -->
                        <span style="display: block" data-bind="text: $data"></span>
                        <!-- /ko -->
                    </div>
                </div>
            </div>
            <div class="info-row content-column-1">
                <span class="info-label">Genre</span>
                <span data-bind="text: genre"></span>
            </div>
            <?php if(is_page('next-showing')) : ?>
                <div class="info-row content-column-1">
                    <span class="info-label">Plot</span>
                    <span data-bind="text: plot"></span>
                </div>
            <?php endif; ?>
            <div class="ratings content-column-1 float-container">
                <div class="content-column-2 alignleft">
                    <div class="rating rating-imdb">
                        <a class="" href="<?php echo 'http://www.imdb.com/title/' . $imdb ?>">
                            <img src="<?php echo get_template_directory_uri() . '/images/IMDb-icon-300x167.png'?>"/>
                        </a>
                        <label data-bind="text: imdbRating"></label>
                    </div>
                    <div class="rating rating-rotten">
                        <a href="http://www.rottentomatoes.com/about/">
                            <img data-bind="attr: {src: getRottenImage}" />
                        </a>
                        <label data-bind="text: getTomatoMeter"></label>
                    </div>
                </div>
                <div class="content-column-2 alignright">
                    <div class="rating rating-itu ">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri()
                                . '/images/ITU.Film-rating.png'?>"/>
                        </a>
                        <?php if(count($reviews) == 0) : ?>
                            <label><?php echo 'N/A'; ?></label>
                        <?php else : ?>
                            <div class="star-rating rating-<?php echo $avg_rating ?>"></div>
                        <?php endif; ?>
                    </div>
                    <div class="rating rating-metacritic ">
                        <a href="http://www.metacritic.com/about-metascores">
                            <img src="<?php echo get_template_directory_uri()
                                . '/images/metacritic_logo.png'?>"/>
                        </a>
                        <label data-bind="text: getMetascore"></label>
                    </div>
                </div>

            </div>
        </div><!-- Movie Info-->
    </div>





