<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package ITUFilm
 */

?>

	</div><!-- #content -->
    </div><!-- #content wrapper -->

	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="footer-nav container-centered">
            <span>
                <a href="http://razi.frwaw.itu.dk/about-us/"><?php printf( __( '%s', 'itufilm' ), 'About Us' ); ?></a>
            </span>
            <span class="sep"> | </span>
            <span>
                <a href="http://razi.frwaw.itu.dk/roster/"><?php printf( __( '%s', 'itufilm' ), 'Roster' ); ?></a>
            </span>
            <span class="sep"> | </span>


            <span>
                <a href="http://razi.frwaw.itu.dk/blog/"><?php printf( __( '%s', 'itufilm' ), 'Blog' ); ?></a>
            </span>
            <span class="sep small-screen"> | </span>
            <br class="small-screen">
            <span>
                <a href="http://razi.frwaw.itu.dk/previous-events/"><?php printf( __( '%s', 'itufilm' ), 'Previous Events' ); ?></a>
            </span>
            <span class="sep"> | </span>
            <span class="icon">
                <a href="https://www.facebook.com/itu.film">
                    <img src="<?php echo get_template_directory_uri().'/images/fbblack.png'?>"/>
                </a>
            </span>
            <span class="sep"> | </span>
            <span class="icon round">
                <a href="https://twitter.com/">
                    <img src="<?php echo get_template_directory_uri().'/images/twitter_circle_black-512.png'?>"/>
                </a>
            </span>
        </div><!-- .site-info -->
		<div class="site-info container-centered">
			<a href="<?php echo esc_url( __( 'http://razi.frwaw.itu.dk/', 'itufilm' ) ); ?>"><?php printf( __( '%s &#169 2015', 'itufilm' ), 'ITUFilm' ); ?></a>
			<span class="sep"> // </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'itufilm' ), 'ITUFilm', 'Rasmus Zimmer Nielsen' ); ?>
		</div><!-- .site-info -->
        <div>Icons made by <a href="http://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>             is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
