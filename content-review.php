
<?php
$rating = get_post_meta($post->ID, '_rating', true) * 10;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="entry-header">
    <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
        <div class="entry-meta">
            <span>Reviewed by <b><?php the_author(); ?></b> on <?php the_time('F j, Y'); ?></span>,
            <div class="star-rating rating-<?php echo $rating ?>"></div>
        </div><!-- .entry-meta -->
</header><!-- .entry-header -->

<div class="entry-content">
    <?php
    the_excerpt();
    ?>
</div><!-- .entry-content -->

</article><!-- #post-## -->
