<?php

// Query: first upcoming event
$today = date('Y-m-d');
$args = array(
    'post_type' => 'events',
    'posts_per_page' => 1,
    'meta_key' => '_date',
    'meta_compare' => '>=',
    'meta_value' => date('Y/m/d'),
    'orderby' => '_date',
    'order' => 'ASC'
);

$loop = new WP_Query( $args );
?>

<?php get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            while ( $loop->have_posts() ) : $loop->the_post();
                get_template_part( 'content', 'next-showing' );
            endwhile;
            ?>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>