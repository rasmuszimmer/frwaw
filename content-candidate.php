<?php
$imdb = get_post_meta($post->ID, "_imdb", true);
$user_voted_id = get_user_meta(get_current_user_id(), 'voted', true);
$bindingID = 'candidate-event-' . $post->ID;
$user_voted_this = $user_voted_id == $post->ID;
$vote_btn_title = ($user_voted_this ? 'You already voted for this title' : 'Vote for this title');
?>

<div id="<?php echo $bindingID ?>" class="candidate-upcoming-event upcoming-event" >
    <div data-bind="fadeVisible: donePopulating">
        <div>
            <span class="suggested-by">Suggested by</span>
            <span><?php the_author()?></span>
        </div>
        <a href="<?php echo get_permalink()?>">
            <img class="image" data-bind="attr: {src: posterUrl, title: title}"/>
        </a>
        <div class="movie-info">
            <div class="movie-title module line-clamp">
                <span class="title" style="display: block" data-bind="text: title"></span>
            </div>
            <div class="year">
                <span style="display: block" data-bind="text: year"></span>
            </div>
            <?php if(is_user_logged_in()) : ?>
                <span title="<?php echo $vote_btn_title  ?>" id="<?php echo $post->ID ?>-vote" class="js-voting <?php echo ($user_voted_this ? 'disabled' : '') ?>" >
                    <img class="js-vote-btn vote-btn" src="<?php echo get_template_directory_uri() . '/images/checkround-24-512.png' ?>">
                </span>
           <?php endif; ?>

        </div>
    </div>
    <img data-bind="visible: !donePopulating()" class="loading-indicator"
         src="<?php echo get_template_directory_uri()?>/images/ajax-loader.gif"/>

</div>

<script>
    require(["movieDataFetcher"], function(mdf) {
        var apiOptions = {
            posterSize: 'w342',
            shortPlot: false,
            fullPlot: false
        };
        mdf.AddBindingTask("<?php echo $imdb ?>", "<?php echo $bindingID ?>", apiOptions);
    });
</script>