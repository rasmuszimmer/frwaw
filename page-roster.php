<?php

$args = array(
    'post_type' => 'staff',
    'meta_key' => '_rank',
    'orderby' => 'meta_value_num',
    'order' => 'ASC'
);

$loop = new WP_Query( $args );

?>

<?php get_header(); ?>


    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <h1><?php the_title();?></h1>
            <hr/>
            <?php
            while ( $loop->have_posts() ) : $loop->the_post();
                get_template_part( 'content', 'staff' );
            endwhile;
            ?>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>