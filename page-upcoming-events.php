<?php get_header(); ?>

    <div id="primary" class="content-area" data-bind="stopBinding: true">
        <main id="main" class="site-main" role="main">
            <?php get_template_part( 'content', 'upcoming-events' ); ?>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>