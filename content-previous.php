<?php
$date = get_post_meta($post->ID, "_date", true);
$time = get_post_meta($post->ID, "_time", true);
$imdb = get_post_meta($post->ID, "_imdb", true);
$fblink = get_post_meta($post->ID, "_fblink", true);
$formattedDate = date("F j", strtotime($date));
$formattedTime = date('g:i A', strtotime($time));
$bindingID = 'planned-upcoming-event-' . $post->ID;
?>

<div id="<?php echo $bindingID ?>" class="planned-upcoming-event upcoming-event" >
    <div data-bind="fadeVisible: donePopulating" class="might-overflow">
        <span class="event-title" title="<?php echo the_title()?>"><?php echo the_title()?></span>
        <div class="date-time">
            <span class="date"><?php echo $formattedDate?></span>
            <span>//</span>
            <span class="time"><?php echo $formattedTime?></span>
            <?php if( !empty($fblink) ) : ?>
                <a class="icon" href="<?php echo $fblink ?>">
                    <img src="<?php echo get_template_directory_uri().'/images/fbblack.png'?>"/>
                </a>
            <?php endif; ?>
        </div>
        <a href="<?php echo get_permalink()?>">
            <img class="image" data-bind="attr: {src: posterUrl, title: title}" />
        </a>
        <div class="movie-info">
            <div class="movie-title module line-clamp">
                <span class="title" style="display: block" data-bind="text: title"></span>
            </div>
            <div class="year">
                <span style="display: block" data-bind="text: year"></span>
            </div>
        </div>
    </div>
    <img data-bind="visible: !donePopulating()" class="loading-indicator"
         src="<?php echo get_template_directory_uri()?>/images/ajax-loader.gif"/>
</div>
<script>
    require(["movieDataFetcher"], function(mdf) {
        var mq = window.matchMedia( "(min-width: 401px)" );
        var apiOptions = {
            posterSize: mq.matches ? 'w342' : 'w342',
            shortPlot: false,
            fullPlot: false
        };
        mdf.AddBindingTask("<?php echo $imdb ?>", "<?php echo $bindingID ?>", apiOptions);
    });
</script>