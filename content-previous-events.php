
<?php
$today = date('Y/m/d');
$args = array(
    'post_type' => 'events',
    'meta_query' => array('relation' => 'AND',
        array('relation' => 'OR',
            'key' => '_date',
            'compare' => '<',
            'value' => $today
        ),
        array(
            'key' => '_is_candidate',
            'compare' => 'NOT EXISTS'
        )
    ),
    'orderby' => '_date',
    'order' => 'DESC'
);

$previous = new WP_Query( $args );
?>

<div class="content-column-1 float-container leading trailing">
    <h1>Previous Events</h1>
    <hr/>


    <div class="planned-events content-column-1 float-container">
        <?php
        while ( $previous->have_posts() ) : $previous->the_post();
            get_template_part( 'content', 'previous' );
        endwhile;
        ?>
    </div>
</div>