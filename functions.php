<?php
/**
 * ITUFilm functions and definitions
 *
 * @package ITUFilm
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'itufilm_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function itufilm_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ITUFilm, use a find and replace
	 * to change 'itufilm' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'itufilm', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

    //This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'itufilm' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
//	add_theme_support( 'custom-background', apply_filters( 'itufilm_custom_background_args', array(
//		'default-color' => 'fff',
//		'default-image' => '',
//	) ) );
}
endif; // itufilm_setup
add_action( 'after_setup_theme', 'itufilm_setup' );

//------------------------------------------------ CUSTOM POST TYPES ------------------------------------------------

/**
 * Create Event post type
 */
function create_events_post_type() {
    register_post_type( 'events',
        array(
            'labels' => array(
                'name' => __( 'Events' ),
                'singular_name' => __( 'Event' ),
                'add_new' => __( 'Add New Event' ),
                'add_new_item' => __( 'Add New Event' ),
                'edit_item' => __( 'Edit Event' ),
                'new_item' => __( 'Add New Event' ),
                'view_item' => __( 'View Event' ),
                'search_items' => __( 'Search Event' ),
                'not_found' => __( 'No events found' ),
                'not_found_in_trash' => __( 'No events found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'thumbnail', 'comments', 'editor' ),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "events"), // Permalinks format
            'menu_position' => 5,
            'register_meta_box_cb' => 'add_events_metaboxes'
        )
    );
    flush_rewrite_rules();
}
add_action( 'init', 'create_events_post_type' );

/**
 * Add metaboxes to the events type
 */
function add_events_metaboxes() {
    add_meta_box('ituf_events_is_candidate', 'Is candidate?', 'ituf_events_is_candidate', 'events', 'normal', 'high');
    add_meta_box('ituf_events_date', 'Date & Time', 'ituf_events_date', 'events', 'normal', 'high');
    add_meta_box('ituf_events_venue', 'Venue', 'ituf_events_venue', 'events', 'normal', 'high');
    add_meta_box('ituf_events_imdb_id', 'IMDB ID', 'ituf_events_imdb_id', 'events', 'normal', 'high');
    add_meta_box('ituf_events_fblink', 'Facebook Event Link', 'ituf_events_fblink', 'events', 'normal', 'high');
}

function ituf_events_is_candidate() {
    global $post;

    // Get the data if its already been entered
    $is_candidate = get_post_meta($post->ID, '_is_candidate', true);

    // Echo out the field
    echo '<p>Candidate?: <input id="is-candidate" type="checkbox" name="_is_candidate"';
    if ($is_candidate) { echo 'checked'; }
    echo '/></p>';

}

function ituf_events_date() {
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';


    // Get the data if its already been entered
    $date = get_post_meta($post->ID, '_date', true);
    $time = get_post_meta($post->ID, '_time', true);

    // Echo out the field
    echo '<p>Date: <input type="text" name="_date" value="' . $date  . '" id="event-date" required/>(yy/mm/dd)</p>';
    echo '<p>Time: <input type="text" name="_time" value="' . $time  . '" id="event-time" required/> (hh:mm)</p>';

}

function ituf_events_venue() {
    global $post;

    // Get the location data if its already been entered
    $venue = get_post_meta($post->ID, '_venue', true);

    // Echo out the field
    echo '<input type="text" name="_venue" placeholder="Where is the event going to take place?" value="'
        . $venue  . '" id="event-venue" class="widefat" required/>';

}

function ituf_events_imdb_id() {
    global $post;

    // Get the location data if its already been entered
    $_imdb = get_post_meta($post->ID, '_imdb', true);

    // Echo out the field
    echo '<p><input type="text" name="_imdb" placeholder="IMDB ID?" value="'
        . $_imdb  . '" id="event-imdb" required/>(ex: tt2515034)</p>';

}

function ituf_events_fblink() {
    global $post;

    // Get the location data if its already been entered
    $fblink = get_post_meta($post->ID, '_fblink', true);

    // Echo out the field
    echo '<input type="text" name="_fblink" placeholder="Is there a facebook event for this?" value="'
        . $fblink  . '" id="event-fblink" class="widefat" />';

}

function ituf_save_events_meta($post_id, $post) {

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    $events_meta['_date'] = $_POST['_date'];
    $events_meta['_time'] = $_POST['_time'];
    $events_meta['_venue'] = $_POST['_venue'];
    $events_meta['_imdb'] = $_POST['_imdb'];
    $events_meta['_fblink'] = $_POST['_fblink'];
    $events_meta['_is_candidate'] = $_POST['_is_candidate'];

    save_or_update_meta($events_meta, $post);
}
add_action('save_post', 'ituf_save_events_meta', 1, 2);

function create_movie_review_post_type() {
    register_post_type( 'review',
        array(
            'labels' => array(
                'name' => __( 'Reviews' ),
                'singular_name' => __( 'Review' ),
                'add_new' => __( 'Add New Review' ),
                'add_new_item' => __( 'Add New Review' ),
                'edit_item' => __( 'Edit Review' ),
                'new_item' => __( 'Add New Review' ),
                'view_item' => __( 'View Review' ),
                'search_items' => __( 'Search Reviews' ),
                'not_found' => __( 'No reviews found' ),
                'not_found_in_trash' => __( 'No reviews found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'thumbnail', 'author' , 'editor'),
            'capability_type' => 'review',
            'capabilities' => array(
                'publish_posts' => 'publish_reviews',
                'edit_posts' => 'edit_reviews',
                'edit_others_posts' => 'edit_others_reviews',
                'delete_posts' => 'delete_reviews',
                'delete_others_posts' => 'delete_others_reviews'
            ),
            'rewrite' => array("slug" => "review"), // Permalinks format
            'menu_position' => 6,
            'register_meta_box_cb' => 'add_movie_reviews_metaboxes'
        )
    );
    flush_rewrite_rules();
}
add_action( 'init', 'create_movie_review_post_type' );

function add_movie_reviews_metaboxes() {
    add_meta_box('ituf_movie_reviews_imdb_id', 'IMDB ID', 'ituf_movie_reviews_imdb_id', 'review', 'normal', 'high');
    add_meta_box('ituf_movie_reviews_rating', 'Rating', 'ituf_movie_reviews_rating', 'review', 'normal', 'high');
}

function ituf_movie_reviews_imdb_id() {
    global $post;

    echo '<input type="hidden" name="reviewmeta_noncename" id="reviewmeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

    $_imdb = get_post_meta($post->ID, '_imdb', true);

    if(get_current_screen()->action == 'add'){
        if(isset($_GET["imdb"])){
            $_imdb = $_GET["imdb"];
        }
    }

    echo '<p><input type="text" name="_imdb" placeholder="IMDB ID?" value="'
        . $_imdb  . '" id="review-imdb" required/>(ex: tt2515034)</p>';

}

function ituf_movie_reviews_rating() {
    global $post;

    $rating = get_post_meta($post->ID, '_rating', true);

    echo '<p><input type="number" min="0" max="5" name="_rating" value="'
        . $rating  . '" id="review-rating" required/>(ITU.film scale: 0-5 )</p>';

}

function ituf_save_movie_reviews_meta($post_id, $post) {

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['reviewmeta_noncename'], plugin_basename(__FILE__) )) {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    $review_meta['_imdb'] = $_POST['_imdb'];
    $review_meta['_rating'] = $_POST['_rating'];

    save_or_update_meta($review_meta, $post);
}
add_action('save_post', 'ituf_save_movie_reviews_meta', 1, 2);

function create_staff_post_type() {
    register_post_type( 'staff',
        array(
            'labels' => array(
                'name' => __( 'Staff' ),
                'singular_name' => __( 'Staff Member' ),
                'add_new' => __( 'Add New Staff Member' ),
                'add_new_item' => __( 'Add New Staff Member' ),
                'edit_item' => __( 'Edit Staff Member' ),
                'new_item' => __( 'Add New Staff Member' ),
                'view_item' => __( 'View Staff Member' ),
                'search_items' => __( 'Search Staff' ),
                'not_found' => __( 'No staff found' ),
                'not_found_in_trash' => __( 'No staff found in trash' )
            ),
            'show_ui' => true,
            'supports' => array( 'title', 'thumbnail'),
            'capability_type' => 'staff',
            'capabilities' => array(
                'publish_posts' => 'publish_staff',
                'edit_posts' => 'edit_staff',
                'edit_others_posts' => 'edit_others_staff',
                'delete_posts' => 'delete_staff',
                'delete_others_posts' => 'delete_others_staff'
            ),
            'rewrite' => array("slug" => "staff"), // Permalinks format
            'menu_position' => 6,
            'register_meta_box_cb' => 'add_staff_metaboxes'
        )
    );
    flush_rewrite_rules();
}
add_action( 'init', 'create_staff_post_type' );

function add_staff_metaboxes() {
    add_meta_box('ituf_staff_rank', 'Rank', 'ituf_staff_rank', 'staff', 'normal', 'high');
    add_meta_box('ituf_staff_study_program', 'Study Program', 'ituf_staff_study_program', 'staff', 'normal', 'high');
    add_meta_box('ituf_staff_desc', 'Description', 'ituf_staff_desc', 'staff', 'normal', 'high');
}

function ituf_staff_rank() {
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="staffmeta_noncename" id="staffmeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

    // Get the location data if its already been entered
    $rank = get_post_meta($post->ID, '_rank', true);

    // Echo out the field
    echo '<p><input type="number" min="1" name="_rank" placeholder="Input staff rank here" value="'
        . $rank . '" id="staff-rank" required/> (Staff rank is used for sorting staff on the Roster page and so is relative)</p>';

}

function ituf_staff_study_program() {
    global $post;

    // Get the location data if its already been entered
    $study = get_post_meta($post->ID, '_study', true);

    // Echo out the field
    echo '<p><input type="text" name="_study" placeholder="Study program?" value="'
        . $study  . '" id="staff-study" class="widefat" required/></p>';

}

function ituf_staff_desc() {
    global $post;

    // Get the location data if its already been entered
    $desc = get_post_meta($post->ID, '_desc', true);

    echo '<textarea id="desc_editor" name="_desc" rows="15" placeholder="Describe the staff member." class="widefat">'
        . $desc . '</textarea>';

    wp_editor( $desc, 'desc_editor');

}

function ituf_save_staff_meta($post_id, $post) {

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['staffmeta_noncename'], plugin_basename(__FILE__) )) {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    $staff_meta['_rank'] = $_POST['_rank'];
    $staff_meta['_study'] = $_POST['_study'];
    $staff_meta['_desc'] = $_POST['_desc'];

    save_or_update_meta($staff_meta, $post);
}
add_action('save_post', 'ituf_save_staff_meta', 1, 2);

//------------------------------- CAPABILITIES -------------------------------------------


function reviews_cap_map( $caps, $cap, $user_id, $args ) {

    if ( 'edit_review' == $cap || 'delete_review' == $cap || 'read_review' == $cap ) {
        $post = get_post( $args[0] );
        $post_type = get_post_type_object( $post->post_type );

        /* Set an empty array for the caps. */
        $caps = array();
    }

    if ( 'edit_review' == $cap ) {
        if ( $user_id == $post->post_author )
            $caps[] = $post_type->cap->edit_posts;
        else
            $caps[] = $post_type->cap->edit_others_posts;
    }

    elseif ( 'delete_review' == $cap ) {
        if ( $user_id == $post->post_author )
            $caps[] = $post_type->cap->delete_posts;
        else
            $caps[] = $post_type->cap->delete_others_posts;
    }

    elseif ( 'read_review' == $cap ) {

        if ( 'private' != $post->post_status )
            $caps[] = 'read';
        elseif ( $user_id == $post->post_author )
            $caps[] = 'read';
        else
            $caps[] = $post_type->cap->read_private_posts;
    }

    return $caps;
}
add_filter( 'map_meta_cap', 'reviews_cap_map', 10, 4 );

function add_caps_to_reviews(){
    global $pagenow;

    $admin = get_role( 'administrator' );
    $editor = get_role('editor');
    $author = get_role('author');
    $contributor = get_role('contributor');
    $subscriber = get_role('subscriber');

    if ( 'themes.php' == $pagenow && isset( $_GET['activated'] ) ){ // Test if theme is activate

        $admin->add_cap( 'publish_reviews' );
        $admin->add_cap( 'edit_reviews' );
        $admin->add_cap( 'edit_others_reviews' );
        $admin->add_cap( 'delete_reviews' );
        $admin->add_cap( 'delete_others_reviews' );
        $admin->add_cap( 'read_private_reviews' );

        $editor->add_cap( 'publish_reviews' );
        $editor->add_cap( 'edit_reviews' );
        $editor->add_cap( 'edit_others_reviews' );
        $editor->add_cap( 'delete_reviews' );
        $editor->add_cap( 'read_private_reviews' );

        $author->add_cap( 'publish_reviews' );
        $author->add_cap( 'edit_reviews' );
        $author->add_cap( 'delete_reviews' );

        $contributor->add_cap( 'publish_reviews' );
        $contributor->add_cap( 'edit_reviews' );
        $contributor->add_cap( 'delete_reviews' );

        $subscriber->add_cap( 'publish_reviews' );
        $subscriber->add_cap( 'edit_reviews' );
        $subscriber->add_cap( 'delete_reviews' );
    }
    else {
        $admin->remove_cap( 'publish_reviews' );
        $admin->remove_cap( 'edit_reviews' );
        $admin->remove_cap( 'edit_others_reviews' );
        $admin->remove_cap( 'delete_reviews' );
        $admin->remove_cap( 'delete_others_reviews' );
        $admin->remove_cap( 'read_private_reviews' );

        $editor->remove_cap( 'publish_reviews' );
        $editor->remove_cap( 'edit_reviews' );
        $editor->remove_cap( 'edit_others_reviews' );
        $editor->remove_cap( 'delete_reviews' );
        $editor->remove_cap( 'read_private_reviews' );

        $author->remove_cap( 'publish_reviews' );
        $author->remove_cap( 'edit_reviews' );
        $author->remove_cap( 'delete_reviews' );

        $contributor->remove_cap( 'publish_reviews' );
        $contributor->remove_cap( 'edit_reviews' );
        $contributor->remove_cap( 'delete_reviews' );

        $subscriber->remove_cap( 'publish_reviews' );
        $subscriber->remove_cap( 'edit_reviews' );
        $subscriber->remove_cap( 'delete_reviews' );
    }
}
add_action( 'load-themes.php', 'add_caps_to_reviews' );

//function staff_cap_map( $caps, $cap, $user_id, $args ) {
//
//    if ( 'edit_staff' == $cap || 'delete_staff' == $cap || 'read_staff' == $cap ) {
//        $post = get_post( $args[0] );
//        $post_type = get_post_type_object( $post->post_type );
//
//        /* Set an empty array for the caps. */
//        $caps = array();
//    }
//
//    if ( 'edit_staff' == $cap ) {
//        if ( $user_id == $post->post_author )
//            $caps[] = $post_type->cap->edit_posts;
//        else
//            $caps[] = $post_type->cap->edit_others_posts;
//    }
//
//    elseif ( 'delete_staff' == $cap ) {
//        if ( $user_id == $post->post_author )
//            $caps[] = $post_type->cap->delete_posts;
//        else
//            $caps[] = $post_type->cap->delete_others_posts;
//    }
//
//    elseif ( 'read_staff' == $cap ) {
//
//        if ( 'private' != $post->post_status )
//            $caps[] = 'read';
//        elseif ( $user_id == $post->post_author )
//            $caps[] = 'read';
//        else
//            $caps[] = $post_type->cap->read_private_posts;
//    }
//
//    return $caps;
//}
//add_filter( 'map_meta_cap', 'staff_cap_map', 10, 4 );

function add_caps_to_staff(){
    global $pagenow;

    $admin = get_role( 'administrator' );

    if ( 'themes.php' == $pagenow && isset( $_GET['activated'] ) ){ // Test if theme is activate

        $admin->add_cap( 'publish_staff' );
        $admin->add_cap( 'edit_staff' );
        $admin->add_cap( 'edit_others_staff' );
        $admin->add_cap( 'delete_staff' );
        $admin->add_cap( 'delete_others_staff' );
        $admin->add_cap( 'read_private_staff' );
    }
    else {
        $admin->remove_cap( 'publish_staff' );
        $admin->remove_cap( 'edit_staff' );
        $admin->remove_cap( 'edit_others_staff' );
        $admin->remove_cap( 'delete_staff' );
        $admin->remove_cap( 'delete_others_staff' );
        $admin->remove_cap( 'read_private_staff' );
    }
}
add_action( 'load-themes.php', 'add_caps_to_staff' );


/**
 * @param $events_meta
 * @param $post
 * Utility for saving meta on custom post types
 */
function save_or_update_meta($events_meta, $post){
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }
}

//------------------------------------------- SCRIPT + STYLE REGISTRATION -----------------------------------

function load_jquery_related_scripts (){
    wp_enqueue_script( 'jquery' );

    wp_register_style( 'jquery-ui', get_template_directory_uri()
        . '/layouts/jquery_ui/jquery-ui.min.css', array(), '20150321', 'all' );
    wp_register_style( 'jquery-ui-structure', get_template_directory_uri()
        . '/layouts/jquery_ui/jquery-ui.structure.min.css', array(), '20150321', 'all' );
    wp_register_style( 'jquery-ui-theme', get_template_directory_uri()
        . '/layouts/jquery_ui/jquery-ui.theme.min.css', array(), '20150321', 'all' );
    wp_enqueue_style('jquery-ui');
    wp_enqueue_style('jquery-ui-structure');
    wp_enqueue_style('jquery-ui-theme');

    wp_enqueue_script( 'jquery-ui-js', get_template_directory_uri()
        . '/js/jquery_ui/jquery-ui.min.js', array(), '20120206', true );
}

/**
 * Enqueue scripts and styles.
 */
function itufilm_scripts() {

    // Main CSS
    wp_enqueue_style( 'itufilm-style', get_stylesheet_uri() );

    // Underscores provided scripts
	wp_enqueue_script( 'itufilm-navigation', get_template_directory_uri()
        . '/js/underscores/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'itufilm-skip-link-focus-fix', get_template_directory_uri()
        . '/js/underscores/skip-link-focus-fix.js', array(), '20130115', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '20150326' );
    wp_enqueue_script( 'voting', get_template_directory_uri() . '/js/voting.js', array(), '20150326', true );
    wp_enqueue_script( 'search', get_template_directory_uri() . '/js/search.js', array(), '20150401', true );

    wp_localize_script( 'voting', 'localizedVars', array(
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'nonce'     => wp_create_nonce( 'nonce' ))
    );
}
add_action( 'wp_enqueue_scripts', 'itufilm_scripts' );

function add_requirejs() {
    $uri = get_template_directory_uri();
    echo '<script>var imagesPath = "'.$uri.'/images/"</script>';
    echo "\n";
    echo '<script>var jsPath = "'.$uri.'/js/"</script>';
    echo "\n";
    echo '<script type="text/javascript" src="'.$uri.'/js/require.js"></script> ';
    echo "\n";

}

if(!is_admin())
    add_action('wp_print_scripts', 'add_requirejs');

/**
 * Add Admin scripts and styles
 */
function load_admin_styles_scripts() {
    load_jquery_related_scripts();

    wp_enqueue_script( 'admin', get_template_directory_uri() . '/js/admin.js', array(), '20150321', true );

    wp_localize_script( 'admin', 'localizedVars', array(
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'nonce'     => wp_create_nonce( 'nonce' ))
    );
}
add_action( 'admin_enqueue_scripts', 'load_admin_styles_scripts' );

//------------------------------------------------ USER REGISTRATION MODIFICATIONS--------------------------------

add_filter( 'registration_errors', 'ensure_itu_domain', 10, 3 );
function ensure_itu_domain( $errors, $sanitized_user_login, $user_email ) {

    $split = explode("@",$user_email);
    $domain = $split[1];
    if($domain != "itu.dk")
        $errors->add( 'domain_error', __( '<strong>ERROR</strong>: Only itu.dk email accounts can be registered.', 'mydomain' ) );
    return $errors;
}

//------------------------------------------------ REQUIRE OTHER PHP FILES----------------------------------------

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Simple DOM.
 */
require get_template_directory() . '/inc/simple_html_dom.php';

//------------------------------------------------ MISC--------------------------------
// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
    global $post;
    $type = get_post_type();
    return $more. '<div><a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full '.$type.'...</a><div>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function round_to_nearest_half($number) {
    return round($number * 2) / 2;
}

function my_login_logo() { ?>
    <style type="text/css">
        .login h1 {
            height: 83px;
        }
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
            padding-bottom: 30px;
            background-size: 320px;
            width: 100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// ---------------------------------------------- VOTING ------------------------------------------

add_action( 'wp_ajax_movie-vote', 'vote_received' );
add_action( 'wp_ajax_clear-votes', 'clear_votes' );

function clear_votes(){
    // check nonce
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'nonce' ) )
        die ( 'Busted!');

    header( "Content-Type: application/json" );

    $args1 = array(
        'post_type' => 'events',
        'meta_query' => array(
            array(
                'key' => '_is_candidate',
                'value' => 'on'
            )
        )
    );

    $all_votes = array();

    $candidates = new WP_Query( $args1 );
    $all_candidates = $candidates->posts;
    foreach ($all_candidates as $candidate) {
        update_post_meta($candidate->ID, 'votes', 0);
        $all_votes[$candidate->ID] = get_post_meta($candidate->ID, 'votes', true);
    }


    foreach (get_users() as $user) {
        delete_user_meta($user->ID, 'voted');
    }

    // generate the response
    $response = json_encode( array(
        'votes' => $all_votes
    ));

    // response output

    echo $response;

    exit;
}

function vote_received(){
// check nonce
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'nonce' ) )
        die ( 'Busted!');

    $postID = $_POST['postID'];
    $user_id = get_current_user_id();

    // Get the ID of the movie the user has voted on
    $user_voted_id = get_user_meta($user_id, 'voted', true);

    header( "Content-Type: application/json" );

    if($postID == $user_voted_id){
        echo json_encode(array(
            'message' => 'Cant vote more than once'
        ));
        exit;
    }

    $votes = get_post_meta($postID, 'votes', true);
    $new_title_votes = 0;

    $prev_title_votes = get_post_meta($user_voted_id, 'votes', true);
    $new_prev_title_votes = 0;

    // User has not voted yet
    if(empty($user_voted_id)){

        // Update user meta
        $success = update_user_meta($user_id, 'voted', $postID);
        if(!$success){
            echo json_encode(array(
                'message' => 'Update user meta failed'
            ));
            exit;
        }

        // Update post meta
        $new_title_votes = (empty($votes) ? 1 : $votes + 1);
        $success = update_post_meta($postID,'votes', $new_title_votes);
        if(!$success){
            echo json_encode(array(
                'message' => 'Update post meta failed'
            ));
            exit;
        }
        // User has previously voted
    }else{

        // Decrement votes for previous voted movie
        $new_prev_title_votes = $prev_title_votes - 1;
        $success = update_post_meta($user_voted_id, 'votes', $new_prev_title_votes);
        if(!$success){
            echo json_encode(array(
                'message' => 'Update post meta failed'
            ));
            exit;
        }

        // Update user meta for new voted movie
        $success = update_user_meta($user_id, 'voted', $postID);
        if(!$success){
            echo json_encode(array(
                'message' => 'Update user meta failed'
            ));
            exit;
        }

        // Update post meta for num votes
        $new_title_votes = (empty($votes) ? 1 : $votes + 1);
        $success = update_post_meta($postID, 'votes', $new_title_votes);
        if(!$success){
            echo json_encode(array(
                'message' => 'Update post meta failed'
            ));
            exit;
        }
    }

    // generate the response
    $response = json_encode( array(
        'user' => get_userdata($user_id)->user_login,
        'change' => array(
            'previous' => array(
                'id' => $user_voted_id,
                'title' => get_the_title($user_voted_id),
                'prevVotes' => $prev_title_votes,
                'votes' => $new_prev_title_votes
            ),
            'new' => array(
                'id' => $postID,
                'title' => get_the_title($postID),
                'prevVotes' => $votes,
                'votes' => $new_title_votes
            )
        )
    ));

    // response output

    echo $response;

    exit;
}

// Function that outputs the contents of the dashboard widget
function dashboard_widget_function( $post, $callback_args ) {
    $args1 = array(
        'post_type' => 'events',
        'meta_query' => array(
            array(
                'key' => '_is_candidate',
                'value' => 'on'
            )
        )
    );

    $candidates = new WP_Query( $args1 );
    if(isset($candidates->posts)) {
        echo '<table class="admin-votes">
                <thead>
                    <th style="text-align: left">Event Title</th>
                    <th style="text-align: right">Votes</th>
                </thead>';
    }else{
        echo 'No candidate events found...';
        return;
    }

    $counter = 0;
    foreach ($candidates->posts as $candidate) {
        $counter++;
        $row_class = ($counter % 2 == 0 ? 'even' : 'odd');
        $event_name = get_the_title($candidate->ID);
        $num_votes = get_post_meta($candidate->ID, 'votes', true);
        echo    '<tr class="'.$row_class.'">
                    <td class="event">'.$event_name.'</td>
                    <td class="num-votes">'.$num_votes.'</td>
                </tr>';
    }

    echo '</table>';
    echo '<button style="margin-top: 10px" id="admin-reset-votes" type="submit">Reset</button>';
    echo '<style>
                table.admin-votes {
                    width: 100%;
                    border-collapse: collapse;
                }
                table.admin-votes thead {
                    border-bottom: 1px solid;
                }
                table.admin-votes td {
                    padding: 10px;
                }
                table.admin-votes tr.even {
                    background-color: #CEE1E8;
                }
                table.admin-votes tr.odd {
                    background-color: #999999;
                }
                table.admin-votes tr td.event {
                    border-right: 1px solid;
                }
                table .num-votes {
                    text-align: right;
                }
        </style>';
}

// Function used in the action hook
function add_dashboard_widgets() {

    // Check if user is admin
    if ( current_user_can( 'manage_options' ) ) {
        wp_add_dashboard_widget('dashboard_widget', 'Candidate Event Voting Summary', 'dashboard_widget_function');
    }
}

// Register the new dashboard widget with the 'wp_dashboard_setup' action
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );

//----------------------------------------------- Search

add_action( 'wp_ajax_nopriv_search', 'search' );
add_action( 'wp_ajax_search', 'search' );

function title_filter($where, &$wp_query){
    global $wpdb;

    if($search_term = $wp_query->get( 'event_title' )){
        /*using the esc_like() in here instead of other esc_sql()*/
        $search_term = $wpdb->esc_like($search_term);
        $search_term = ' \'%' . $search_term . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE '.$search_term;
    }

    return $where;
}

function search_events_by_title($search_term){
    $args = array(
        'post_type' => 'events',
        'event_title' => $search_term,
        'post_status' => 'publish',
        'orderby'     => 'title',
        'order'       => 'ASC'
    );

    add_filter( 'posts_where', 'title_filter', 10, 2 );
    $wp_query = new WP_Query($args);
    remove_filter( 'posts_where', 'title_filter', 10, 2 );
    return $wp_query->posts;
}

function search_events_by_imdb_id($imdb_id){
    $args = array(
        'post_type' => 'events',
        'meta_key' => '_imdb',
        'meta_value' => $imdb_id,
        'meta_compare' => '=',
        'orderby' => '_date',
        'order' => 'ASC'
    );
    $wp_query = new WP_Query($args);
    return $wp_query->posts;
}

function get_imdb_id_from_title($search_term){
    global $wpdb;
    $table_name = $wpdb->prefix . 'imdb_id_to_title2';


    $results = $wpdb->get_results(
        "
	SELECT imdbid
	FROM $table_name
	WHERE title LIKE '%$search_term%'
	"
    );
    return $results;
}

function map_search_result($result){
    return array(
        'title' => $result -> post_title,
        'href' => home_url() . '/' . $result -> post_name,
        'date' => get_post_meta($result -> ID, '_date', true),
        'isCandidate' => get_post_meta($result -> ID, '_is_candidate', true)
    );
}

function search(){
//    $nonce = $_GET['nonce'];
//    if ( ! wp_verify_nonce( $nonce, 'nonce' ) )
//        die ( 'Busted!');

    $search_term = $_GET['searchTerm'];

    $results = search_events_by_title($search_term);
    $imdb_ids = get_imdb_id_from_title($search_term);

    if(!empty($imdb_ids))
        $events_by_imdb_id = search_events_by_imdb_id($imdb_ids[0]->imdbid);

    // TODO: Handle case where search term matches more than 1 IMDB ID

    // Filter events that were already found in the normal title query
    foreach ($events_by_imdb_id as $event) {
        if(!in_array($event, $results))
            array_push($results, $event);
    }

    $response = json_encode( array(
        'results' => array_map('map_search_result', $results),
    ));

    // response output
    header( "Content-Type: application/json" );
    echo $response;

    exit;
}

add_action( 'wp_ajax_save_omdb_data', 'save_omdb_data' );

function save_omdb_data(){
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'nonce' ) )
        die ( 'Busted!');

    $data = $_POST['data'];

    global $wpdb;
    $table_name = $wpdb->prefix . 'imdb_id_to_title2';

    foreach ($data as $entry) {
        $wpdb->insert(
            $table_name,
            array(
                'imdbid' => $entry['imdbID'],
                'title' => $entry['title'],
                'title_year' => $entry['year']
            ),
            array(
                '%s',
                '%s',
                '%d'
            )
        );
    }

    $results = $wpdb->get_results( 'SELECT * FROM '.$table_name.'', OBJECT );

    $response = json_encode( array(
        'results' => $results
    ));

    // response output
    header( "Content-Type: application/json" );
    echo $response;

    exit;
}

//function setup_title_to_imdb_id_storage() {
//    global $wpdb;
//
//    $table_name = $wpdb->prefix . 'imdb_id_to_title2';
//
//    $charset_collate = $wpdb->get_charset_collate();
//    $sql = "CREATE TABLE $table_name (
//		imdbid varchar (10) ,
//		title text NOT NULL,
//		title_year year (4) NOT NULL,
//		UNIQUE KEY imdbid (imdbid)
//	) $charset_collate;";
//
//    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
//    dbDelta( $sql );
//}
//
//add_action( 'load-themes.php', 'setup_title_to_imdb_id_storage' );