<?php
$date = get_post_meta($post->ID, "_date", true);
$time = get_post_meta($post->ID, "_time", true);
$venue = get_post_meta($post->ID, "_venue", true);
$formattedDate = date("F j", strtotime($date));
$formattedTime = date('g:i A', strtotime($time));
$imdb = get_post_meta($post->ID, "_imdb", true);
$fblink = get_post_meta($post->ID, "_fblink", true);
$bindingID = 'post-' . $post->ID;
?>



<div class ="page-header">
    <div class="content-column-1 float-container leading trailing">
        <div class="alignleft content-column-2 leading">
            <h1 class="alignleft">Next Showing</h1>
        </div>
        <div class="small-screen alignright content-column-2 trailing">
            <h1 class="alignright">

                <span style="text-transform: uppercase; font-weight: bolder"><?php echo $formattedDate ?></span>
                <span style="margin: 0 10px">//</span>
                <br class="small-screen">
                <span style="color: red; font-weight: bolder"><?php echo $formattedTime ?></span>

            </h1>
        </div>
    </div>

    <div class="content-column-1 float-container leading trailing">
        <div class="alignleft content-column-2 leading">
            <h2 class="entry-title" style="display: inline-block;"><?php the_title()?></h2>
            <?php if( !empty($fblink) ) : ?>
                <a class="icon" href="<?php echo $fblink ?>">
                    <img src="<?php echo get_template_directory_uri().'/images/fbblack.png'?>"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="alignleft content-column-2 trailing">
            <h2 class="small-screen alignright trailing"><?php echo $venue ?></h2>
        </div>
    </div>
    <hr/>
</div>

<div id="<?php echo $bindingID; ?>">
    <img data-bind="visible: !donePopulating()" class="loading-indicator"
         src="<?php echo get_template_directory_uri()?>/images/ajax-loader.gif"/>
    <article  <?php post_class(); ?> data-bind="fadeVisible: donePopulating" style="display: none">
        <?php
            get_template_part('content', 'single-event-short');
        ?>
    </article><!-- #post-## -->
</div>
<script>
    require(["movieDataFetcher"], function(mdf) {
        var mq = window.matchMedia( "(min-width: 401px)" );
        var apiOptions = {
            posterSize: mq.matches ? 'w780' : 'w500',
            fullPlot: false
        };
        mdf.AddBindingTask("<?php echo $imdb ?>", "<?php echo $bindingID; ?>", apiOptions);
    });
</script>