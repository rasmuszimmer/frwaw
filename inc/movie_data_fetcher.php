<?php
$tmdb_api_key = '0143aac658ba7c7bd8536dd58bd4c1d3';
$tmdb_config = file_get_contents('http://api.themoviedb.org/3/configuration?api_key=' . $tmdb_api_key);
$decoded_tmdb_config = json_decode($tmdb_config, true);
/**
 * Created by PhpStorm.
 * User: RasmusZimmer
 * Date: 22-03-2015
 * Time: 22:59
 */
function get_movie_data($imdbID, $poster_size, $backdrop_size, $trailer_count = '0'){
    global $decoded_tmdb_config, $tmdb_api_key;

    $omdb = file_get_contents('http://www.omdbapi.com/?i='. $imdbID .'&tomatoes=true');
    $omdb_full_plot = file_get_contents('http://www.omdbapi.com/?i='. $imdbID .'&tomatoes=true&plot=full');
    $tmdb = file_get_contents('http://api.themoviedb.org/3/find/'. $imdbID .'?api_key='
        . $tmdb_api_key .'&external_source=imdb_id');
    $decoded_tmdb = json_decode($tmdb, true);
    $decoded_omdb = json_decode($omdb, true);
    $decoded_omdb_full_plot = json_decode($omdb_full_plot, true);

    $tmdb_img_data = $decoded_tmdb_config['images'];
    $tmdb_movie_result = $decoded_tmdb['movie_results'][0];
    $movie_result_poster_path = $tmdb_movie_result['poster_path'];
    $movie_result_backdrop_path = $tmdb_movie_result['backdrop_path'];

    $poster_url = $tmdb_img_data['base_url'] . $poster_size . $movie_result_poster_path;
    $backdrop_url = $tmdb_img_data['base_url'] . $backdrop_size . $movie_result_backdrop_path;

    // Strip leading letters from imdbID
    $imdb_trimmed = substr($imdbID, 2);

    $trailer_parsed = simplexml_load_file('http://api.traileraddict.com/?imdb='. $imdb_trimmed .'&count='
        . $trailer_count . '&width=000');
    $trailer_embeds = array();
    foreach($trailer_parsed->trailer as $x => $trailer)
    {
        array_push($trailer_embeds, $trailer -> embed);
    }

    $movie_data = array(
        'data' => $decoded_omdb,
        'full_plot' => $decoded_omdb_full_plot['Plot'],
        'poster_url' => $poster_url,
        'backdrop_url' => $backdrop_url,
        'trailer_embeds' => $trailer_embeds
    );

    return $movie_data;
}

?>