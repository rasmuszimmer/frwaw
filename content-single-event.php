<?php
/**
 * Created by PhpStorm.
 * User: RasmusZimmer
 * Date: 23-03-2015
 * Time: 16:58
 */

$date = get_post_meta($post->ID, "_date", true);
$time = get_post_meta($post->ID, "_time", true);
$formattedDate = date("F j", strtotime($date));
$formattedTime = date('g:i A', strtotime($time));
$venue = get_post_meta($post->ID, "_venue", true);
$is_candidate = get_post_meta($post->ID, "_is_candidate", true);
$fblink = get_post_meta($post->ID, "_fblink", true);
$imdb = get_post_meta($post->ID, "_imdb", true);
$imdb_trimmed = substr($imdb, 2);
$trailer_parsed = simplexml_load_file('http://api.traileraddict.com/?imdb='. $imdb_trimmed .'&count=3&width=000');
$embeds = array();
foreach($trailer_parsed->trailer as $x => $trailer)
{
    array_push($embeds, $trailer -> embed);
}

// set add review global
$GLOBALS['itu_film_globals']['add_new_review_imdb_id'] = $imdb;
?>

<script>
    require(["movieDataFetcher"], function(mdf) {
        var mq = window.matchMedia( "(min-width: 401px)" );
        var apiOptions = {
            posterSize: mq.matches ? 'w780' : 'w500',
            backdropSize: mq.matches ? 'w1280' : 'w780',
            shortPlot: false
        };
        mdf.AddBindingTask("<?php echo $imdb ?>", "page", apiOptions);
    });
</script>

<div class ="page-header">
    <div class="content-column-1 float-container leading trailing">
        <div class="alignleft content-column-2 leading">
            <h2 class="alignleft" ">
            <?php if($is_candidate == 'on') : ?>
                <h3>Suggested by <span><?php the_author()?></span></h3>
            <?php else : ?>
                <span><?php the_title() ?></span>
            <?php endif; ?>

            <?php if( !empty($fblink) ) : ?>
                <a class="icon" href="<?php echo $fblink ?>">
                    <img src="<?php echo get_template_directory_uri().'/images/fbblack.png'?>"/>
                </a>
            <?php endif; ?>
            </h2>
        </div>
        <div class="alignright content-column-2 trailing">
            <h3 class="alignright">
                <span style="text-transform: uppercase; font-weight: bolder"><?php echo $formattedDate ?></span>
                <span style="margin: 0 10px"> @</span>
                <span style="color: red; font-weight: bolder"><?php echo $formattedTime . ','?></span>
                <span><?php echo $venue ?></span>
            </h3>
        </div>
    </div>

    <hr/>
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <img data-bind="visible: !donePopulating()" class="loading-indicator"
         src="<?php echo get_template_directory_uri()?>/images/ajax-loader.gif"/>
    <div class="entry-content" data-bind="fadeVisible: donePopulating">
        <?php
        get_template_part('content', 'single-event-short');
        ?>

        <div class="additional-movie-info content-column-1 float-container leading">
            <div class="movie-synopsis">
                <h3 class="info-label">Synopsis</h3>
                <p style="text-align: justify" data-bind="text: synopsis"></p>
            </div>
        </div>


        <h3 class="info-label">Trailers</h3>
        <div class="trailers content-column-1 trailing leading" style="display: inline-flex">
            <?php if(count($embeds) == 0) echo 'Sorry, no trailers found :( '?>
            <?php foreach($embeds as $embed): ?>
                <div class="trailer" style="width: 50%">
                    <?php echo $embed ?>
                </div>
            <?php endforeach; ?>
        </div>

        <?php
        wp_link_pages( array(
            'before' => '<div class="page-links">' . __( 'Pages:', 'itufilm' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->

    <div class="comments-container content-column-1 float-container leading trailing">
        <div class="alignleft content-column-2 leading">
            <h3 class="info-label">Comments</h3>
            <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            ?>
        </div>

        <?php
        $args = array(
            'post_type' => 'review',
            'meta_key' => '_imdb',
            'meta_compare' => '=',
            'meta_value' => $imdb,
            'orderby' => 'date',
            'order' => 'ASC'
        );

        $loop = new WP_Query( $args );
        ?>

        <?php $linkdata = array(
            'link_text' => 'Review this title',
            'link_title' => 'Write your own review for this title',
            'link_url' => 'http://razi.frwaw.itu.dk/wp-admin/post-new.php?post_type=review&imdb='. $imdb,
            'link_id' => 'write-new-review'
        );

        ?>

        <div class="alignright content-column-2">

            <h3 style="float: left" class="info-label">User Reviews</h3>
            <a  title="<?php echo $linkdata['link_title'] ?>" style="float: right; margin-top: 20px"
                id="<?php echo $linkdata['link_id']; ?>" href="<?php echo $linkdata['link_url']; ?>">
                <?php echo $linkdata['link_text']; ?>
            </a>

            <?php
            while ( $loop->have_posts() ) : $loop->the_post();
                get_template_part( 'content', 'review' );
            endwhile;
            wp_reset_query();
            ?>
        </div>
    </div>


    <footer class="entry-footer">
        <?php itufilm_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->


