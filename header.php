<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package ITUFilm
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php
    $showQuote = is_page('next-showing');
    if($showQuote){
        $html = file_get_html('http://en.wikipedia.org/wiki/AFI%27s_100_Years...100_Movie_Quotes');
        $quotes = array();
        foreach ( $html->find('table.wikitable tr') as $tr ) {
            array_push($quotes, array(
                'quotation' => $tr->find('td', 1)->plaintext,
                'character' => $tr->find('td', 2)->plaintext,
                'actor' => $tr->find('td', 3)->plaintext,
                'film' => $tr->find('td', 4)->plaintext,
                'year' => $tr->find('td', 5)->plaintext
            ));
        }

        //Pick random quote
        $randquote = $quotes[array_rand($quotes)];
    }
    ?>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site" data-bind="style: { backgroundImage: getBackgroundImage }">

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'itufilm' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
        <?php if ( get_header_image() ) : ?>
            <div class="nav-logo alignleft">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
                </a>
            </div>
        <?php endif; // End header image check. ?>
		<span class="site-branding alignleft">
            <?php if ( !get_header_image() ) : ?>
			    <span class="site-title alignleft"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
            <?php endif; // End header image check. ?>
			<span class="site-description alignleft"><?php bloginfo( 'description' ); ?></span>
		</span><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
<!--			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">--><?php //_e( 'Primary Menu', 'itufilm' ); ?><!--</button>-->
            <div class="large-standard-menu">
                <div class="events-search-wrapper" data-bind="stopBinding: true">
                    <div id="event-search">
                        <input type="search" placeholder="Search all events" class="search-box" data-bind="textInput: searchString" />

                    </div>
                </div>
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
                <div class="nav-login">
                    <ul>
                        <li><?php wp_loginout($_SERVER['REQUEST_URI'] ); ?> <span class="sep"> | </span> <?php wp_register(' ' , ' '); ?></li>
                    </ul>
                </div>
            </div>
            <div class="hidden-icon-menu">
                <ul>
                    <li>
                        <a href="http://razi.frwaw.itu.dk/upcoming-events/">
                            <img src="<?php echo get_template_directory_uri() . '/images/daily-calendar2.png' ?>"/>
                        </a>

                    <li>
                        <a href="http://razi.frwaw.itu.dk/suggest-movie/">
                            <img src="<?php echo get_template_directory_uri() . '/images/inspiration.png' ?>"/>
                        </a>
                    </li>
                    <li>
                        <?php if(is_user_logged_in()) : ?>
                            <a href="<?php echo wp_logout_url($_SERVER['REQUEST_URI']); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/logout11.png' ?>"/>
                            </a>
                        <?php else : ?>
                            <a href="<?php echo wp_login_url($_SERVER['REQUEST_URI']); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/login10.png' ?>">
                            </a>
                        <?php endif; ?>
                    </li>
                    <li>
                        <?php if(is_user_logged_in()) : ?>
                            <a href="<?php echo admin_url(); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/gear39.png' ?>"/>
                            </a>
                        <?php else : ?>
                            <a href="<?php echo wp_registration_url(); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/add40.png' ?>">
                            </a>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
    <?php if ($showQuote) : ?>
        <div id="quote-of-the-day">
            <blockquote class="quotation">
                <h3>
                    <?php echo $randquote['quotation']; ?>
                </h3>
                <hr/>
                <p class="end">
                    <span> - <?php echo $randquote['character']; ?></span>
                </p>
                <p style="text-align: center">
                    <span>
                        <?php echo $randquote['actor']; ?> in the <?php echo $randquote['year'] ?>
                        movie: <br/> <b><?php echo '"' . $randquote['film'] . '"' ?></b>
                    </span>
                </p>
            </blockquote>
        </div>
    <?php else: ?>
        <div class="padding-fix"></div>
    <?php endif; ?>
    <div data-bind="stopBinding: true">
        <div class="search-results-wrapper" data-bind="slideVisible: hasResults, duration: 300, callback: animationDone" style="display: none">
            <ul data-bind="foreach: results">
                <li class="search-result">
                    <a data-bind="attr: {href: href}">
                        <span style="word-wrap: break-word; " data-bind="text: title"></span>
<!--                        <span>(<span data-bind="text: movieTitle" />)</span>-->
                        <img data-bind="visible: $parent.isCandidate($data)" src="<?php echo get_template_directory_uri() . '/images/vote1.png'?>">
                        <p data-bind="text: date"></p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div id="content-wrapper">
	<div id="content" class="site-content container">
