<?php
/**
 * Created by PhpStorm.
 * User: RasmusZimmer
 * Date: 27-03-2015
 * Time: 19:27
 */

$study = get_post_meta($post->ID, "_study", true);
$desc = get_post_meta($post->ID, "_desc", true);
?>

<div class="staff-member float-container">
    <?php
    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
        echo '<div class="alignleft thumb">';
        the_post_thumbnail( 'medium' );
    }   echo '</div>'
    ?>
    <div class="alignright info">
        <h2><?php the_title() ?></h2>
        <h3><?php echo $study; ?></h3>
        <p>
            <?php echo $desc; ?>
        </p>
    </div>
</div>